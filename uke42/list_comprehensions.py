# Lage en liste med alle tall 0 - 9 ganget med seg selv:
liste = []
for i in range(10):
    liste.append(i*i)
    
print(liste)

# Dette er det samme, bare raskere form:
liste2 = [i*i for i in range(10)]

print(liste2)

# Vi kan legge inn sjekker også - bare oddetall:
liste3 = [i*i for i in range(10) if i % 2]

print(liste3)