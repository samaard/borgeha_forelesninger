liste = ['a','e','i','o','u','y','æ','ø','å']
streng = 'aeiouyæøå'

# Plukke ut enkelttegn
tegn1 = liste[2]
tegn2 = streng[2]
print(f'{tegn1} {tegn2}')

# Teste med operatoren in
if 'e' in streng:
    print('Vokal')
    
# Konvertere streng til liste
nyliste = list(streng)
print(nyliste)

# slice en streng
print(streng[2:])