# Disse funksjonene viser at en kan kalle funksjoner fra andre.
# hovedprogrammer kallet main()
# main kaller finn_streng_og_plass()
# så kaller den skriv_bokstav()

def finn_streng_og_plass():
    while True:
        streng = input('Skriv inn en streng: ')
        plass = int(input(f'Tall mellom 0 og {len(streng)-1}: '))
        if plass > len(streng) -1:
            continue
        else:
            return streng, plass
        
        
def skriv_bokstav(strengen, valg):
    print(f'På plass nummer {valg} i strengen '\
          f'{strengen} kommer {strengen[valg]}')
    
def main():
    tekst, tall = finn_streng_og_plass()
    skriv_bokstav(tekst, tall)
    
main()
